from io import BytesIO

from spotseeker_server.test import SpotServerTestCase


class ImageTestCase(SpotServerTestCase):

    def upload_image(self, image_file_name, url, extra_args=None):
        c = self.client
        with open(image_file_name, 'rb') as f:
            img = BytesIO(f.read())
            img.name = image_file_name
            res_args = {'image': img}
            if extra_args:
                res_args.update(extra_args)
            response = c.post(url, data=res_args)

        return response
