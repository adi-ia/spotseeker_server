#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='spotseeker_server',
      version='0.0.4',
      description='REST Backend for SpaceScout',
      packages=find_packages(),
      include_package_data=True,
      install_requires=[
                        'Django',
                        'mock',
                        'Pillow',
                        'pyproj',
                        'pytz',
                        'simplejson',
                        'phonenumbers'
                       ],
     )
