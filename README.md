# spotseeker_server

spotseeker_server is a fork of [spotseeker_server](https://github.com/uw-it-aca/spotseeker_server)
by the University of Washington Information Technology's Academic Experience Design & Delivery group.
It has been updated to be compatible with Python 3 and Django 2.

## Requirements

* Python 3.6
* Pipenv
* pyenv

### Developer Setup (for macOS)

spotseeker_server is a Django app, not a project, and as such, needs to be installed into an
actual project. [WiScout](https://git.doit.wisc.edu/andrew-summers/wiscout) is that project,
and the instructions below will set up a development environment with WiScout that can be used
to work on spotseeker_server.

1. Clone WiScout to your machine

2. Install spotseeker_server as an editable dependency.

    ```sh
    # from wiscout inside project
    $ pipenv install -e /path/to/spotseeker_server
    ```

3. Add spotseeker_server to `INSTALLED_APPS`

    ```py
    # wiscout/settings.py
    INSTALLED_APPS = [
        ...
        'spotseeker_server',
        ....
    ]
    ```

4. Run migrations

    ```sh
    # from inside wiscout project switch to pipenv shell. 
    $ pipenv shell
    $ python manage.py migrate
    ```

You can now make changes to spotseeker_server and they will reflected automatically in the WiScout project.

## Sample Spots

To add Sample Spots to the API (inside pipenv shell):

```sh
$ python manage.py create_sample_spots
```

Note that this currently skips sample images.
